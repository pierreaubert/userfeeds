#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from userfeeds.appuserfeeds import create_userfeeds

# create the application for WSGI server
app = create_userfeeds()

# nice for debugging
if __name__ == '__main__':
    app.run(host=app.config['USERFEEDS_HOST'],
            port=app.config['USERFEEDS_PORT'],
            debug=app.config['DEBUG'])

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import os
import opml
from flask import g, current_app, request, jsonify, render_template
from flask.views import MethodView
from flask.ext.uploads import UploadNotAllowed
from utils import json_status as js
from models import UserFeeds
from sso.client import authenticate
from helpers import db_get_conn
from unidecode import unidecode
from werkzeug import secure_filename


class UserOPMLAPI(MethodView):
    """
    # ------------------------------------------------------------------
    # UserReads
    # GET    /opml
    #                return the list of user feed in opml format
    # POST   /opml
    #                take an opml file, parse it and inject it  
    # ------------------------------------------------------------------
    """
    def _process_feed_grid(self, url, category):
        """ add feed_id in user feeds list at category
        """
        rssfeed_id = current_app.rssfeeds_client.feed_get_id(url)
        if rssfeed_id != -1:
            # print u'adding user_id={0} rss_feed_id={1} category={2}'.\
            #    format(g.user_id, rssfeed_id, category)
            db_conn = db_get_conn()
            db_userfeeds = UserFeeds(db_conn)
            error = db_userfeeds.put(g.user_id,
                                     rssfeed_id,
                                     category, None, None)

    def _doublequote(self, s):
        """ replace ' by ''
        """
        return s.replace("'", "''")


    def _process_opml(self, filename):
        """ read and parse the opml file
        for each feed insert them in crawler database and in user grid
        """
        error = None
        # read and parse file
        filename = secure_filename(filename.filename)
        path = os.path.join(current_app.config['UPLOADED_OPML_DEST'],
                            filename)
        feeds = opml.parse(path)
        # how many new, old and error feeds in this opml file?
        counter_new = 0
        counter_old = 0
        counter_err = 0
        for f in feeds:
            url = ''
            category = f.text
            if len(f) > 0:
                for sf in f:
                    if len(sf) == 0:
                        url = sf.xmlUrl
                        if 'title' in sf:
                            sqltitle = self.doublequote(
                                unidecode(unicode(sf.title)))
                        else:
                            sqltitle = ''
                        counter = current_app.rssfeeds_client.\
                                  feed_post(url, sqltitle)
                        # if successfully create the feed
                        if counter >= 0:
                            if counter > 0:
                                counter_new += 1
                            else:
                                counter_old += 1
                            # add the feed to user grid
                            self._process_feed_grid(url, category)
                        else:
                            counter_err += 1
                    # print 'c='+str(counter)+' ' + url
            else:
                url = f.xmlUrl
                if 'title' in f:
                    sqltitle = self.doublequote(
                        unidecode(unicode(f.title)))
                else:
                    sqltitle = ''
                counter = current_app.rssfeeds_client.\
                          feed_post(url, sqltitle)
                # if successfully create the feed
                if counter >= 0:
                    if counter > 0:
                        counter_new += 1
                    else:
                        counter_old += 1
                    self._process_feed_grid(url, category)
                else:
                    counter_err += 1
            # print 'c='+str(counter)+' ' + url

        return counter_err, counter_old, counter_new

    @authenticate
    def post(self):
        """ load the opml file and inject it
        """
        error = None
        if 'opml' in request.files:
            opmlfile = request.files['opml']
            try:
                current_app.opml.save(opmlfile)
                # 1. create the feeds we do not have
                nb_err, nb_old, nb_new = self._process_opml(opmlfile)
                imported = {
                    'new': nb_new,
                    'old': nb_old,
                    'err': nb_err
                }
                resp = {
                    'status': 'ok',
                    'import': imported
                }
                return jsonify(resp)
            except UploadNotAllowed:
                error = 'Upload not allowed'
        else:
            error = 'filename must be provided'
        return js.json_status(error)

    @authenticate
    def get(self):
        """ build a opml (xml file) from user feeds list
        """
        error = None
        db_conn = db_get_conn()
        db_userfeeds = UserFeeds(db_conn)
        grid = db_userfeeds.get(g.user_id)
        title = 'My feeds @7pi'
        outlines = []
        for category in grid:
            feeds = []
            for feed in grid[category]:
                # get id
                rssfeed_id = feed['rssfeed_id']
                # ask info to feed db
                db = current_app.rssfeeds_client.feed_get(rssfeed_id)
                if 'title' in db and 'url' in db:
                    # append to list of feeds in this outline
                    feeds.append({
                        'text': db['title'],
                        'title': db['title'],
                        'xmlUrl': db['url'],
                        'htmlUrl': db['url'],
                    })
            outlines.append({
                'text': category,
                'title': category,
                'feeds': feeds
            })
        return render_template('opml.xml',
                               title=title,
                               outlines=outlines)

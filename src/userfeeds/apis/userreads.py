#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from flask import jsonify
from flask.views import MethodView
from sso.helpers import json_status as js

from sso.client.decorators.dflask import authenticate
from userfeeds.helpers import db_get_conn
from userfeeds.models.userreads import UserReads


class UserReadsAPI(MethodView):
    """
    # ------------------------------------------------------------------
    # UserReads
    # GET    /userreads/<userfeed_id>
    #                return the list of all read items in this userfeed
    # POST   /userreads/<userfeed_id>/<rssitem_id>
    #                mark item read in userfeed
    # DELET  /userreads/<userfeed_id>/<rssitem_id>
    #                mark item as unread in userfeed
    # ------------------------------------------------------------------
    """
    @authenticate
    def get(self, userfeed_id):
        """
        """
        error = None
        db_conn = db_get_conn()
        db_userreads = UserReads(db_conn)
        lor = db_userreads.get(userfeed_id)
        if lor is not None:
            resp = {
                'status': 'ok',
                'reads': lor
            }
            return jsonify(resp)
        return js.json_status(error)

    @authenticate
    def post(self, userfeed_id, rssitem_id):
        """
        """
        error = None
        db_conn = db_get_conn()
        db_userreads = UserReads(db_conn)
        error = db_userreads.post(userfeed_id, rssitem_id)
        return js.json_status(error)

    @authenticate
    def delete(self, userfeed_id, rssitem_id):
        """
        """
        error = None
        db_conn = db_get_conn()
        db_userreads = UserReads(db_conn)
        error = db_userreads.delete(userfeed_id, rssitem_id)
        return js.json_status(error)

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from flask import g, jsonify, request
from flask.views import MethodView
from sso.helpers import json_status as js
from sso.client.decorators.dflask import authenticate
from userfeeds.helpers import db_get_conn
from userfeeds.models.userfeeds import UserFeeds


class UserFeedsAPI(MethodView):
    """
    # ------------------------------------------------------------------
    # UserFeeds
    # GET    /userfeeds return the list of all feeds
    # POST   /userfeeds create a list of feeds (for opml import for ex)
    # PUT    /userfeeds add a feed
    # PATCH  /userfeeds modify a feed
    # DELETE /userfeeds delete a feed or all the feeds
    # ------------------------------------------------------------------
    """
    @authenticate
    def get(self):
        """ get users feed """
        error = None
        db_conn = db_get_conn()
        db_userfeeds = UserFeeds(db_conn)
        lof = db_userfeeds.get(g.userinfo['user_id'])
        if lof is not None:
            resp = {
                'status': 'ok',
                'grid': lof
            }
            return jsonify(resp)
        return js.json_status(error)

    @authenticate
    def post(self, feed_id):
        """ nothing to do """
        error = None
        return js.json_status(error)

    @authenticate
    def put(self, feed_id):
        """ add feed_id to list of current user feeds list"""
        error = None
        db_conn = db_get_conn()
        db_userfeeds = UserFeeds(db_conn)
        tag1 = None
        tag2 = None
        tag3 = None
        if 'tag1' in request.form:
            tag1 = request.form['tag1']
        if 'tag2' in request.form:
            tag2 = request.form['tag2']
        if 'tag3' in request.form:
            tag3 = request.form['tag3']
        error = db_userfeeds.put(g.user_id, feed_id, tag1, tag2, tag3)
        return js.json_status(error)

    @authenticate
    def patch(self, feed_id):
        """ modify feed_id to list of current user feeds list"""
        error = None
        db_conn = db_get_conn()
        db_userfeeds = UserFeeds(db_conn)
        tag1 = None
        tag2 = None
        tag3 = None
        pos = None
        if 'tag1' in request.form:
            tag1 = request.form['tag1']
        if 'tag2' in request.form:
            tag2 = request.form['tag2']
        if 'tag3' in request.form:
            tag3 = request.form['tag3']
        if 'pos' in request.form:
            pos = request.form['pos']
        error = db_userfeeds.patch(g.user_id, feed_id, pos, tag1, tag2, tag3)
        return js.json_status(error)

    @authenticate
    def delete(self, feed_id):
        """ delete a feed from userlist or all if feed_id is None """
        error = None
        # to be done
        return js.json_status(error)

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from datetime import datetime
from itertools import groupby
from sqlalchemy import Table, Column, Integer, String, \
    UniqueConstraint, MetaData, Sequence, Index, DateTime, ForeignKey, \
    select, insert, delete
from sqlalchemy.exc import NoSuchTableError, SQLAlchemyError, \
    OperationalError, IntegrityError
from userfeeds.models.meta import userfeeds_meta

userreads = Table('userreads', 
                  userfeeds_meta,
                  Column('userread_id', 
                         Integer,
                         Sequence('userreads_seq'),
                         primary_key=True),
                  Column('userfeed_id',
                         Integer,
                         nullable=False),
                  Column('rssitem_id',
                         Integer,
                         nullable=False),
                  UniqueConstraint('userfeed_id',
                                   'rssitem_id',
                                   name='userreads_userfeedrssitem_uniq'))

Index('userreads_user_idx', userreads.c.userfeed_id)


class UserReads():
    """ UserReads take care of items already read by user
    # ------------------------------------------------------------------
    # 
    # ------------------------------------------------------------------
    """

    def __init__(self, conn):
        """ set connection and lastrowid
        """
        self.conn = conn
        self.lastrowid = -1

    def getall(self, user_id):
        """ return all items read by a user
        """
        s = select([userfeeds.c.rssfeed_id, userreads.c.rssitem_id])\
            .where(userreads.c.userfeed_id == userfeeds.c.userfeed_id)\
            .where(userfeeds.c.user_id == user_id)\
            .order_by(userfeeds.c.rssfeed_id)\
            .order_by(userreads.c.rssitem_id.desc())
        # lor is a list of pairs (feed/item)
        lor = self.conn.execute(s).fetchall()
        # group by feed
        sortfeedfn = key = lambda s : s[0]
        r = {}
        for key, valuesiter in groupby(lor, key = sortfeedfn):
            l = [v[1] for v in valuesiter if len(v)>1]
            r[key] = l
        return r

    def get(self, userfeed_id):
        """ return all items read in userfeed
        """
        s = select([userreads.c.rssitem_id])\
            .where(userreads.c.userfeed_id == userfeed_id)\
            .order_by(userreads.c.rssitem_id.desc())
        lor = self.conn.execute(s).fetchall()
        l = [r[0] for r in lor if len(r) > 0]
        return [dict(userfeed_id=userfeed_id, read_items_id=l)]

    def post(self, userfeed_id, rssitem_id):
        """ mark item as read 
        """
        error = None
        ins = userreads.insert().values(userfeed_id=userfeed_id,
                                        rssitem_id=rssitem_id)
        try:
            res = self.conn.execute(ins)
            self.lastrowid = res.lastrowid
        except IntegrityError as e:
            error = 'User already read this item'
        return error

    def delete(self, userfeed_id, item_id):
        """ mark item as unread 
        """
        error = None
        d = userreads.delete()\
                     .where(userreads.c.userfeed_id == userfeed_id)\
                     .where(userreads.c.rssitem_id == item_id)
        try:
            res = self.conn.execute(d)
            self.lastrowid = res.lastrowid
        except IntegrityError as e:
            error = 'User didn\'t read this item before'
        return error


        

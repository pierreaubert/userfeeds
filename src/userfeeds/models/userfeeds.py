#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from datetime import datetime
from itertools import groupby
from sqlalchemy import Table, Column, Integer, String, \
    UniqueConstraint, MetaData, Sequence, Index, DateTime, ForeignKey, \
    select, insert, delete
from sqlalchemy.exc import NoSuchTableError, SQLAlchemyError, \
    OperationalError, IntegrityError
from userfeeds.models.meta import userfeeds_meta

userfeeds = Table('userfeeds', 
                  userfeeds_meta,
                  Column('userfeed_id', 
                         Integer, 
                         Sequence('userfeeds_seq'), 
                         primary_key=True),
                  Column('user_id',
                         Integer,
                         nullable=False),
                  Column('rssfeed_id',
                         Integer,
                         nullable=False),
                  Column('last_read_date',
                         DateTime,
                         nullable=False),
                  Column('pos_in_category',
                         Integer,
                         nullable=False),
                  Column('tag1',
                         Integer,
                         nullable=True),
                  Column('tag2',
                         Integer,
                         nullable=True),
                  Column('tag3',
                         Integer,
                         nullable=True),
                  UniqueConstraint('user_id',
                                   'rssfeed_id',
                                   name='userfeeds_userfeed_uniq'))


Index('userfeeds_userfeed_idx',
      userfeeds.c.user_id, userfeeds.c.rssfeed_id,
      unique=True)


class UserFeeds(object):
    """ UserFeeds models a list of feeds for a user
    # ------------------------------------------------------------------
    # ------------------------------------------------------------------
    """

    def __init__(self, conn):
        self.conn = conn
        self.lastrowid = -1

    def lof(self, userid):
        """ return the user list of feeds
        """
        s = select([userfeeds.c.feed_id])\
            .where(userfeeds.c.user_id == userid)\
            .order_by(userfeeds.c.tag1)
        lof = self.conn.execute(s).fetchall()
        return s, lof

    def lof_details(self, userid):
        """ return the user list of feeds
        """
        s = select([userfeeds])\
            .where(userfeeds.c.user_id == userid)\
            .order_by(userfeeds.c.tag1)
        lof = self.conn.execute(s).fetchall()
        return s, lof

    def lof2grid(self, select, lof, reads):
        """ create a json grid usefull for building a menu of feeds
        """
        # build a dico
        dico = []
        for row in lof:
            feed = {}
            for col, val in zip(select.columns, row):
                feed[col.name] = val
            dico.append(feed)
        # reorder by category to build menu
        results = {}
        sortcat1fn = key = lambda s : s['tag1']
        for key, valuesiter in groupby(dico, key=sortcat1fn):
            feeds = []
            for v in valuesiter:
                rssfeed_id = v['rssfeed_id']
                if rssfeed_id in reads:
                    v['itemreads'] = reads[rssfeed_id]
                feeds.append(v)
            results[key] = feeds
        return results

    def get(self, userid):
        """ return a json grid with current feeds for user userid
        """
        sel, lof = self.lof_details(userid)
        reads = UserReads(self.conn).getall(userid)
        return self.lof2grid(sel, lof, reads)

    def put(self, user_id, feed_id, tag1, tag2, tag3):
        """
        """
        error = None
        now = datetime.utcnow()
        i = userfeeds.insert().values(user_id=user_id,
                                      rssfeed_id=feed_id,
                                      last_read_date=now,
                                      pos_in_category=0,
                                      tag1=tag1,
                                      tag2=tag2,
                                      tag3=tag3)
        try:
            res = self.conn.execute(i)
            self.lastrowid = res.lastrowid
        except IntegrityError as e:
            error = 'User already subscribe to feed'
        return error

    def patch(self, user_id, feed_id, pos, tag1, tag2, tag3):
        """
        """
        error = None
        now = datetime.utcnow()
        u = userfeeds.update()\
                     .where(userfeeds.c.user_id == user_id)\
                     .where(userfeeds.c.rssfeed_id == feed_id)\
                     .values(last_read_date=now)
        if pos is not None:
            u = u.values(pos_in_category=pos)
        if tag1 is not None:
            u = u.values(tag1=tag1)
        if tag2 is not None:
            u = u.values(tag2=tag2)
        if tag3 is not None:
            u = u.values(tag3=tag3)

        try:
            res = self.conn.execute(u)
            self.lastrowid = res.lastrowid
        except IntegrityError as e:
            error = 'User already subscribe to feed'
        return error

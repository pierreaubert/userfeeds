#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from sqlalchemy import Table, Column, Integer, String, \
    UniqueConstraint, MetaData, Sequence, Index, DateTime, ForeignKey, \
    select, insert, delete
from sqlalchemy.exc import NoSuchTableError, SQLAlchemyError, \
    OperationalError, IntegrityError
from userfeeds.models.meta import userfeeds_meta


tags = Table('tags', 
             userfeeds_meta,
             Column('tag_id', Integer, Sequence('tag_seq'), primary_key=True),
             Column('text', String(16), nullable=False),
             UniqueConstraint('text', name='tags_text_uniq'))


Index('tags_tag_idx', tags.c.text)

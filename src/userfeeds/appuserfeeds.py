#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import logging
from flask import Flask, g, current_app, request
from flask.ext.uploads import UploadSet, configure_uploads, \
    patch_request_class, DATA
from sso import client as sso_client
from sso.helpers import db_schema_exist
from rssfeeds.client import rest as rssfeeds_client

# get apis
from userfeeds.apis.userfeeds import UserFeedsAPI
from userfeeds.apis.userreads import UserReadsAPI
from userfeeds.apis.useropml import UserOPMLAPI

# get metas
from userfeeds.models.meta import userfeeds_meta


def db_close_conn(exception):
    conn = getattr(g, 'conn', None)
    if conn is not None:
        # print '(trace) > close conn'
        conn.close()
    # else:
    #    print '(trace) > already closed conn'


def db_check(engine):
    """ create the databases associated with ssi
    """
    if not db_schema_exist(engine, ['userfeeds', 'tags']):
        userfeeds_meta.create_all(engine)

    # from alembic.config import Config
    # from alembic import command
    # alembic_cfg = Config("alembic.ini")
    # command.stamp(alembic_cfg, "head")


def cache_profile_in_g():
    if 'userinfo' not in g:
        sso_cookie = None
        if 'ssosign' in request.cookies:
            sso_cookie = request.cookies['ssosign']
        g.userinfo = current_app.sso_client.get_profile(sso_cookie)


def create_userfeeds():
    """ setup the userfeeds application
    # ------------------------------------------------------------------
    # create app
    # ------------------------------------------------------------------
    """
    userfeeds = Flask('userfeeds')
    userfeeds.config.from_envvar('USERFEEDS_SETTINGS')

    # add a file logger
    userfeeds.logger.setLevel(userfeeds.config['LOG_LEVEL'])
    fh = logging.FileHandler(userfeeds.config['LOG_FILE'])
    fm = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    fh.setFormatter(fm)
    userfeeds.logger.addHandler(fh)

    # if using Sentry add a logger
    if len(userfeeds.config['SENTRY_DSN']) > 0:
        # if you want to use Sentry
        from raven.contrib.flask import Sentry
        from raven.handlers.logging import SentryHandler

        sentry = Sentry(userfeeds, dsn=userfeeds.config['SENTRY_DSN'])
        sentry.init_app(userfeeds)
        sh = SentryHandler(userfeeds.config['SENTRY_DSN'])
        userfeeds.logger.addHandler(sh)

    # fire engine
    from sqlalchemy import create_engine
    userfeeds.userfeeds_engine = create_engine(userfeeds.config['DATABASE_URI'])
    db_check(userfeeds.userfeeds_engine)

    # add routes
    userfeeds_view = UserFeedsAPI.as_view('userfeeds_api')
    userfeeds.add_url_rule('/userfeeds',
                           view_func=userfeeds_view,
                           methods=['GET', 'POST', 'DELETE'])
    userfeeds.add_url_rule('/userfeeds/<int:feed_id>',
                           view_func=userfeeds_view,
                           methods=['PUT', 'PATCH', 'DELETE'])

    userreads_view = UserReadsAPI.as_view('userreads_api')
    userfeeds.add_url_rule('/userreads/<int:userfeed_id>',
                           view_func=userreads_view,
                           methods=['GET'])
    userfeeds.add_url_rule('/userreads/<int:userfeed_id>/<int:rssitem_id>',
                           view_func=userreads_view,
                           methods=['POST', 'DELETE'])

    useropml_view = UserOPMLAPI.as_view('useropml_api')
    userfeeds.add_url_rule('/opml',
                           view_func=useropml_view,
                           methods=['GET', 'POST'])

    # add a teardown appcontext
    userfeeds.teardown_appcontext(db_close_conn)

    # add a before request handler
    userfeeds.before_request(cache_profile_in_g)

    # configure upload via flask-uploads
    def dest(self):
        return userfeeds.config['UPLOADED_OPML_DEST']
    userfeeds.opml = UploadSet(name='ompl',
                               extensions=DATA,
                               default_dest=dest)
    configure_uploads(userfeeds, (userfeeds.opml))
    patch_request_class(userfeeds, 1*1024*1024)

    # add a sso_client
    userfeeds.sso_client = sso_client.Client(
        userfeeds.config['SSO_URL'],
        userfeeds.logger)

    # add a rssfeeds client
    userfeeds.rssfeeds_client = rssfeeds_client.Client(
        userfeeds.config['FEEDS_URL'],
        userfeeds.sso_client,
        userfeeds.logger)

    return userfeeds

# create application
app = create_userfeeds()

if __name__ == '__main__':
    app.run(host=app.config['USERFEEDS_HOST'],
            port=app.config['USERFEEDS_PORT'],
            debug=app.config['DEBUG'])

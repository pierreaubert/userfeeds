#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import logging
from datetime import date

env = {
    # version
    VERSION: "0.02",
    # ROOT_DIR
    ROOT_DIR: "/Users/pierreaubert/Run",
    # debug is false by default
    DEBUG: True,
    # testing is false by default
    TESTING: False,
    # your favorite database, sqlalchimy convention
    #DATABASE_URI: 'sqlite://:memory',
    DATABASE_URI: 'sqlite:///{0}/data/sqlite_dev/userfeeds.db'.format(ROOT_DIR),
    # main log file
    LOG_APP: "userfeeds"
    LOG_FILE: "{0}/var/log/userfeed_dev_{1}.log".format(ROOT_DIR,date.today().isoformat())
    LOG_LEVEL: logging.DEBUG,
    # host/port for userfeeds
    USERFEEDS_PORT: 9082,
    USERFEEDS_HOST: "127.0.0.1",
    # SECRET KEY for debug and dev
    SECRET_KEY: 'dev&debug&are&fun',
    # sso service
    SSO_URL: 'http://localhost:9080',
    # rssfeeds service
    FEEDS_URL: 'http://localhost:9081',
    # DIR for file uploads
    UPLOADED_OPML_DEST: "{0}/var/tmp/opml".format(ROOT_DIR),
    UPLOADED_OPML_ALLOW: ('xml', 'opml'),
    # local sentry
    SENTRY_DSN:'',
}
    



